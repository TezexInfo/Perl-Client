# TezexClient::Object::Origination

## Load the model package
```perl
use TezexClient::Object::Origination;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **string** |  | [optional] 
**branch** | **string** |  | [optional] 
**source** | **string** |  | [optional] 
**public_key** | **string** |  | [optional] 
**fee** | **int** |  | [optional] 
**counter** | **int** |  | [optional] 
**operations** | [**ARRAY[OriginationOperation]**](OriginationOperation.md) |  | [optional] 
**level** | **int** |  | [optional] 
**block_hash** | **string** |  | [optional] 
**time** | **DateTime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



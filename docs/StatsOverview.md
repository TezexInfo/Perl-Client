# TezexClient::Object::StatsOverview

## Load the model package
```perl
use TezexClient::Object::StatsOverview;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_usd** | **string** |  | [optional] 
**price_btc** | **string** |  | [optional] 
**block_time** | **int** | Blocktime in seconds | [optional] 
**priority** | [**Number**](Number.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TezexClient::OriginationApi

## Load the API package
```perl
use TezexClient::Object::OriginationApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_origination**](OriginationApi.md#get_origination) | **GET** /origination/{origination_hash} | Get Origination


# **get_origination**
> Origination get_origination(origination_hash => $origination_hash)

Get Origination

Get a specific Origination

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::OriginationApi->new();
my $origination_hash = 'origination_hash_example'; # string | The hash of the Origination to retrieve

eval { 
    my $result = $api_instance->get_origination(origination_hash => $origination_hash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling OriginationApi->get_origination: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **origination_hash** | **string**| The hash of the Origination to retrieve | 

### Return type

[**Origination**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


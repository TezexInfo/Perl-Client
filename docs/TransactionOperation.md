# TezexClient::Object::TransactionOperation

## Load the model package
```perl
use TezexClient::Object::TransactionOperation;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **string** |  | [optional] 
**amount** | **int** |  | [optional] 
**destination** | **string** |  | [optional] 
**parameters** | [**TezosScript**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



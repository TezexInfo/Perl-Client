# TezexClient::BlockApi

## Load the API package
```perl
use TezexClient::Object::BlockApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blocks_all**](BlockApi.md#blocks_all) | **GET** /blocks/all | Get All Blocks 
[**blocks_by_level**](BlockApi.md#blocks_by_level) | **GET** /blocks/{level} | Get All Blocks for a specific Level
[**blocks_by_level_range**](BlockApi.md#blocks_by_level_range) | **GET** /blocks/{startlevel}/{stoplevel} | Get All Blocks for a specific Level-Range
[**get_block**](BlockApi.md#get_block) | **GET** /block/{blockhash} | Get Block By Blockhash
[**get_block_delegations**](BlockApi.md#get_block_delegations) | **GET** /block/{blockhash}/operations/delegations | Get Delegations of a Block
[**get_block_endorsements**](BlockApi.md#get_block_endorsements) | **GET** /block/{blockhash}/operations/endorsements | Get Endorsements of a Block
[**get_block_operations_sorted**](BlockApi.md#get_block_operations_sorted) | **GET** /block/{blockhash}/operations | Get operations of a block, sorted
[**get_block_originations**](BlockApi.md#get_block_originations) | **GET** /block/{blockhash}/operations/originations | Get Originations of a Block
[**get_block_transaction**](BlockApi.md#get_block_transaction) | **GET** /block/{blockhash}/operations/transactions | Get Transactions of Block
[**recent_blocks**](BlockApi.md#recent_blocks) | **GET** /blocks/recent | returns the last 25 blocks


# **blocks_all**
> BlocksAll blocks_all(page => $page, order => $order, limit => $limit)

Get All Blocks 

Get all Blocks

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $page = 3.4; # Number | Pagination, 200 tx per page max
my $order = 'order_example'; # string | ASC or DESC
my $limit = 56; # int | Results per Page

eval { 
    my $result = $api_instance->blocks_all(page => $page, order => $order, limit => $limit);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->blocks_all: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Pagination, 200 tx per page max | [optional] 
 **order** | **string**| ASC or DESC | [optional] 
 **limit** | **int**| Results per Page | [optional] 

### Return type

[**BlocksAll**](BlocksAll.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blocks_by_level**
> ARRAY[Block] blocks_by_level(level => $level)

Get All Blocks for a specific Level

Get all Blocks for a specific Level

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $level = 3.4; # Number | The level of the Blocks to retrieve, includes abandoned

eval { 
    my $result = $api_instance->blocks_by_level(level => $level);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->blocks_by_level: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **level** | **Number**| The level of the Blocks to retrieve, includes abandoned | 

### Return type

[**ARRAY[Block]**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **blocks_by_level_range**
> BlockRange blocks_by_level_range(startlevel => $startlevel, stoplevel => $stoplevel)

Get All Blocks for a specific Level-Range

Get all Blocks for a specific Level-Range

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $startlevel = 3.4; # Number | lowest blocklevel to return
my $stoplevel = 3.4; # Number | highest blocklevel to return

eval { 
    my $result = $api_instance->blocks_by_level_range(startlevel => $startlevel, stoplevel => $stoplevel);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->blocks_by_level_range: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **Number**| lowest blocklevel to return | 
 **stoplevel** | **Number**| highest blocklevel to return | 

### Return type

[**BlockRange**](BlockRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block**
> Block get_block(blockhash => $blockhash)

Get Block By Blockhash

Get a block by its hash

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $blockhash = 'blockhash_example'; # string | The hash of the Block to retrieve

eval { 
    my $result = $api_instance->get_block(blockhash => $blockhash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->get_block: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| The hash of the Block to retrieve | 

### Return type

[**Block**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_delegations**
> ARRAY[Delegation] get_block_delegations(blockhash => $blockhash)

Get Delegations of a Block

Get all Delegations of a specific Block

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $blockhash = 'blockhash_example'; # string | Blockhash

eval { 
    my $result = $api_instance->get_block_delegations(blockhash => $blockhash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->get_block_delegations: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**ARRAY[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_endorsements**
> ARRAY[Endorsement] get_block_endorsements(blockhash => $blockhash)

Get Endorsements of a Block

Get all Endorsements of a specific Block

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $blockhash = 'blockhash_example'; # string | Blockhash

eval { 
    my $result = $api_instance->get_block_endorsements(blockhash => $blockhash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->get_block_endorsements: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**ARRAY[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_operations_sorted**
> BlockOperationsSorted get_block_operations_sorted(blockhash => $blockhash)

Get operations of a block, sorted

Get the maximum Level we have seen, Blocks at this level may become abandoned Blocks later on

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $blockhash = 'blockhash_example'; # string | The hash of the Block to retrieve

eval { 
    my $result = $api_instance->get_block_operations_sorted(blockhash => $blockhash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->get_block_operations_sorted: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| The hash of the Block to retrieve | 

### Return type

[**BlockOperationsSorted**](BlockOperationsSorted.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_originations**
> ARRAY[Origination] get_block_originations(blockhash => $blockhash)

Get Originations of a Block

Get all Originations of a spcific Block

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $blockhash = 'blockhash_example'; # string | Blockhash

eval { 
    my $result = $api_instance->get_block_originations(blockhash => $blockhash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->get_block_originations: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**ARRAY[Origination]**](Origination.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_block_transaction**
> ARRAY[Transaction] get_block_transaction(blockhash => $blockhash)

Get Transactions of Block

Get all Transactions of a spcific Block

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();
my $blockhash = 'blockhash_example'; # string | Blockhash

eval { 
    my $result = $api_instance->get_block_transaction(blockhash => $blockhash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->get_block_transaction: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **blockhash** | **string**| Blockhash | 

### Return type

[**ARRAY[Transaction]**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **recent_blocks**
> ARRAY[Block] recent_blocks()

returns the last 25 blocks

Get all Blocks for a specific Level

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockApi->new();

eval { 
    my $result = $api_instance->recent_blocks();
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockApi->recent_blocks: $@\n";
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ARRAY[Block]**](Block.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# TezexClient::Object::BlockRange

## Load the model package
```perl
use TezexClient::Object::BlockRange;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_block_level** | **int** |  | [optional] 
**blocks** | [**ARRAY[Block]**](Block.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



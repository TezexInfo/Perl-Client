# TezexClient::Object::Candlestick

## Load the model package
```perl
use TezexClient::Object::Candlestick;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**o** | **string** |  | [optional] 
**h** | **string** |  | [optional] 
**l** | **string** |  | [optional] 
**c** | **string** |  | [optional] 
**vol** | **string** |  | [optional] 
**time** | **DateTime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TezexClient::Object::Operation

## Load the model package
```perl
use TezexClient::Object::Operation;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transaction** | [**Transaction**](Transaction.md) |  | [optional] 
**origination** | [**Origination**](Origination.md) |  | [optional] 
**delegation** | [**Delegation**](Delegation.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



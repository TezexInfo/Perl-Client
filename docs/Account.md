# TezexClient::Object::Account

## Load the model package
```perl
use TezexClient::Object::Account;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**address** | **string** |  | [optional] 
**operation_count** | **int** |  | [optional] 
**sent_transaction_count** | **int** |  | [optional] 
**recv_transaction_count** | **int** |  | [optional] 
**origination_count** | **int** |  | [optional] 
**delegation_count** | **int** |  | [optional] 
**delegated_count** | **int** |  | [optional] 
**endorsement_count** | **int** |  | [optional] 
**first_seen** | **DateTime** |  | [optional] 
**last_seen** | **DateTime** |  | [optional] 
**name** | **string** |  | [optional] 
**balance** | **string** |  | [optional] 
**total_sent** | **string** |  | [optional] 
**total_received** | **string** |  | [optional] 
**baked_blocks** | **int** |  | [optional] 
**image_url** | **string** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



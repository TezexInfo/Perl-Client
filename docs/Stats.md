# TezexClient::Object::Stats

## Load the model package
```perl
use TezexClient::Object::Stats;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**stat_group** | **string** |  | [optional] 
**stat** | **string** |  | [optional] 
**value** | **string** |  | [optional] 
**start** | **DateTime** |  | [optional] 
**end** | **DateTime** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



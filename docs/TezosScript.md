# TezexClient::Object::TezosScript

## Load the model package
```perl
use TezexClient::Object::TezosScript;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**int** | **string** |  | [optional] 
**string** | **string** |  | [optional] 
**prim** | **string** |  | [optional] 
**args** | [**ARRAY[TezosScript]**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TezexClient::DelegationApi

## Load the API package
```perl
use TezexClient::Object::DelegationApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_delegation**](DelegationApi.md#get_delegation) | **GET** /delegation/{delegation_hash} | Get Delegation


# **get_delegation**
> Delegation get_delegation(delegation_hash => $delegation_hash)

Get Delegation

Get a specific Delegation

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::DelegationApi->new();
my $delegation_hash = 'delegation_hash_example'; # string | The hash of the Origination to retrieve

eval { 
    my $result = $api_instance->get_delegation(delegation_hash => $delegation_hash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling DelegationApi->get_delegation: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **delegation_hash** | **string**| The hash of the Origination to retrieve | 

### Return type

[**Delegation**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


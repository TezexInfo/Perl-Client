# TezexClient::Object::BlockOperationsSorted

## Load the model package
```perl
use TezexClient::Object::BlockOperationsSorted;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**transactions** | [**ARRAY[Transaction]**](Transaction.md) |  | [optional] 
**originations** | [**ARRAY[Origination]**](Origination.md) |  | [optional] 
**delegations** | [**ARRAY[Delegation]**](Delegation.md) |  | [optional] 
**endorsements** | [**ARRAY[Endorsement]**](Endorsement.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



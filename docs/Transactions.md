# TezexClient::Object::Transactions

## Load the model package
```perl
use TezexClient::Object::Transactions;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



# TezexClient::NetworkApi

## Load the API package
```perl
use TezexClient::Object::NetworkApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**network**](NetworkApi.md#network) | **GET** /network | Get Network Information


# **network**
> NetworkInfo network()

Get Network Information

Get Network Information

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::NetworkApi->new();

eval { 
    my $result = $api_instance->network();
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling NetworkApi->network: $@\n";
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**NetworkInfo**](NetworkInfo.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


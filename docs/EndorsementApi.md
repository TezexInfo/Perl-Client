# TezexClient::EndorsementApi

## Load the API package
```perl
use TezexClient::Object::EndorsementApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_endorsement**](EndorsementApi.md#get_endorsement) | **GET** /endorsement/{endorsement_hash} | Get Endorsement
[**get_endorsement_for_block**](EndorsementApi.md#get_endorsement_for_block) | **GET** /endorsement/for/{block_hash} | Get Endorsement


# **get_endorsement**
> Endorsement get_endorsement(endorsement_hash => $endorsement_hash)

Get Endorsement

Get a specific Endorsement

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::EndorsementApi->new();
my $endorsement_hash = 'endorsement_hash_example'; # string | The hash of the Endorsement to retrieve

eval { 
    my $result = $api_instance->get_endorsement(endorsement_hash => $endorsement_hash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling EndorsementApi->get_endorsement: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **endorsement_hash** | **string**| The hash of the Endorsement to retrieve | 

### Return type

[**Endorsement**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_endorsement_for_block**
> ARRAY[Endorsement] get_endorsement_for_block(block_hash => $block_hash)

Get Endorsement

Get a specific Endorsement

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::EndorsementApi->new();
my $block_hash = 'block_hash_example'; # string | blockhash

eval { 
    my $result = $api_instance->get_endorsement_for_block(block_hash => $block_hash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling EndorsementApi->get_endorsement_for_block: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **block_hash** | **string**| blockhash | 

### Return type

[**ARRAY[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


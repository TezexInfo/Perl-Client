# TezexClient::Object::TransactionRange

## Load the model package
```perl
use TezexClient::Object::TransactionRange;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**max_block_level** | **int** |  | [optional] 
**transactions** | [**ARRAY[Transaction]**](Transaction.md) |  | [optional] 
**total_results** | **int** |  | [optional] 
**current_page** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



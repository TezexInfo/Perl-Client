# TezexClient::BlockchainApi

## Load the API package
```perl
use TezexClient::Object::BlockchainApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**blockheight**](BlockchainApi.md#blockheight) | **GET** /maxLevel | Get Max Blockheight


# **blockheight**
> Level blockheight()

Get Max Blockheight

Get the maximum Level we have seen

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::BlockchainApi->new();

eval { 
    my $result = $api_instance->blockheight();
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling BlockchainApi->blockheight: $@\n";
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**Level**](Level.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


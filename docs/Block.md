# TezexClient::Object::Block

## Load the model package
```perl
use TezexClient::Object::Block;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**hash** | **string** |  | [optional] 
**net_id** | **string** |  | [optional] 
**protocol** | **string** |  | [optional] 
**level** | **int** |  | [optional] 
**proto** | **string** |  | [optional] 
**successors** | [**ARRAY[ChainStatus]**](ChainStatus.md) |  | [optional] 
**predecessor** | **string** |  | [optional] 
**time** | **DateTime** |  | [optional] 
**validation_pass** | **string** |  | [optional] 
**data** | **string** |  | [optional] 
**chain_status** | **string** |  | [optional] 
**operations_count** | **int** |  | [optional] 
**operations_hash** | **string** |  | [optional] 
**baker** | **string** |  | [optional] 
**seed_nonce_hash** | **string** |  | [optional] 
**proof_of_work_nonce** | **string** |  | [optional] 
**signature** | **string** |  | [optional] 
**priority** | **int** |  | [optional] 
**operation_count** | **int** |  | [optional] 
**total_fee** | **string** |  | [optional] 
**operations** | [**BlockOperationsSorted**](BlockOperationsSorted.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



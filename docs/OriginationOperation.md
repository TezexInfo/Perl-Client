# TezexClient::Object::OriginationOperation

## Load the model package
```perl
use TezexClient::Object::OriginationOperation;
```

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**kind** | **string** |  | [optional] 
**manager_pubkey** | **string** |  | [optional] 
**balance** | **int** |  | [optional] 
**spendable** | **boolean** |  | [optional] 
**delegateable** | **boolean** |  | [optional] 
**delegate** | **string** |  | [optional] 
**script** | [**TezosScript**](TezosScript.md) |  | [optional] 
**storage** | [**TezosScript**](TezosScript.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)



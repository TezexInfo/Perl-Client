# TezexClient::AccountApi

## Load the API package
```perl
use TezexClient::Object::AccountApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_account**](AccountApi.md#get_account) | **GET** /account/{account} | Get Account
[**get_account_balance**](AccountApi.md#get_account_balance) | **GET** /account/{account}/balance | Get Account Balance
[**get_account_last_seen**](AccountApi.md#get_account_last_seen) | **GET** /account/{account}/last_seen | Get last active date
[**get_account_operation_count**](AccountApi.md#get_account_operation_count) | **GET** /account/{account}/operations_count | Get operation count of Account
[**get_account_transaction_count**](AccountApi.md#get_account_transaction_count) | **GET** /account/{account}/transaction_count | Get transaction count of Account
[**get_delegations_for_account**](AccountApi.md#get_delegations_for_account) | **GET** /account/{account}/delegations | Get Delegations of this account
[**get_delegations_to_account**](AccountApi.md#get_delegations_to_account) | **GET** /account/{account}/delegated | Get Delegations to this account
[**get_endorsements_for_account**](AccountApi.md#get_endorsements_for_account) | **GET** /account/{account}/endorsements | Get Endorsements this Account has made
[**get_transaction_for_account_incoming**](AccountApi.md#get_transaction_for_account_incoming) | **GET** /account/{account}/transactions/incoming | Get Transaction
[**get_transaction_for_account_outgoing**](AccountApi.md#get_transaction_for_account_outgoing) | **GET** /account/{account}/transactions/outgoing | Get Transaction


# **get_account**
> Account get_account(account => $account)

Get Account

Get Acccount

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account

eval { 
    my $result = $api_instance->get_account(account => $account);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

[**Account**](Account.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_balance**
> string get_account_balance(account => $account)

Get Account Balance

Get Balance

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account

eval { 
    my $result = $api_instance->get_account_balance(account => $account);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_account_balance: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**string**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_last_seen**
> DateTime get_account_last_seen(account => $account)

Get last active date

Get LastSeen Date

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account

eval { 
    my $result = $api_instance->get_account_last_seen(account => $account);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_account_last_seen: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**DateTime**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_operation_count**
> int get_account_operation_count(account => $account)

Get operation count of Account

Get Operation Count

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account

eval { 
    my $result = $api_instance->get_account_operation_count(account => $account);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_account_operation_count: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_account_transaction_count**
> int get_account_transaction_count(account => $account)

Get transaction count of Account

Get Transaction Count

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account

eval { 
    my $result = $api_instance->get_account_transaction_count(account => $account);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_account_transaction_count: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account | 

### Return type

**int**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_delegations_for_account**
> ARRAY[Delegation] get_delegations_for_account(account => $account, before => $before)

Get Delegations of this account

Get Delegations this Account has made

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account for which to retrieve Delegations
my $before = 56; # int | Only Return Delegations before this blocklevel

eval { 
    my $result = $api_instance->get_delegations_for_account(account => $account, before => $before);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_delegations_for_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve Delegations | 
 **before** | **int**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**ARRAY[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_delegations_to_account**
> ARRAY[Delegation] get_delegations_to_account(account => $account, before => $before)

Get Delegations to this account

Get that have been made to this Account

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account to which delegations have been made
my $before = 56; # int | Only Return Delegations before this blocklevel

eval { 
    my $result = $api_instance->get_delegations_to_account(account => $account, before => $before);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_delegations_to_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account to which delegations have been made | 
 **before** | **int**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**ARRAY[Delegation]**](Delegation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_endorsements_for_account**
> ARRAY[Endorsement] get_endorsements_for_account(account => $account, before => $before)

Get Endorsements this Account has made

Get Endorsements this Account has made

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account for which to retrieve Endorsements
my $before = 56; # int | Only Return Delegations before this blocklevel

eval { 
    my $result = $api_instance->get_endorsements_for_account(account => $account, before => $before);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_endorsements_for_account: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve Endorsements | 
 **before** | **int**| Only Return Delegations before this blocklevel | [optional] 

### Return type

[**ARRAY[Endorsement]**](Endorsement.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transaction_for_account_incoming**
> Transactions get_transaction_for_account_incoming(account => $account, before => $before)

Get Transaction

Get incoming Transactions for a specific Account

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account for which to retrieve incoming Transactions
my $before = 56; # int | Only Return transactions before this blocklevel

eval { 
    my $result = $api_instance->get_transaction_for_account_incoming(account => $account, before => $before);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_transaction_for_account_incoming: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve incoming Transactions | 
 **before** | **int**| Only Return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transaction_for_account_outgoing**
> Transactions get_transaction_for_account_outgoing(account => $account, before => $before)

Get Transaction

Get outgoing Transactions for a specific Account

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::AccountApi->new();
my $account = 'account_example'; # string | The account for which to retrieve outgoing Transactions
my $before = 56; # int | Only return transactions before this blocklevel

eval { 
    my $result = $api_instance->get_transaction_for_account_outgoing(account => $account, before => $before);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling AccountApi->get_transaction_for_account_outgoing: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **account** | **string**| The account for which to retrieve outgoing Transactions | 
 **before** | **int**| Only return transactions before this blocklevel | [optional] 

### Return type

[**Transactions**](Transactions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


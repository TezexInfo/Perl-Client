# TezexClient::TransactionApi

## Load the API package
```perl
use TezexClient::Object::TransactionApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_transaction**](TransactionApi.md#get_transaction) | **GET** /transaction/{transaction_hash} | Get Transaction
[**get_transactions_recent**](TransactionApi.md#get_transactions_recent) | **GET** /transactions/recent | Returns the last 50 Transactions
[**transactions_all**](TransactionApi.md#transactions_all) | **GET** /transactions/all | Get All Transactions
[**transactions_by_level_range**](TransactionApi.md#transactions_by_level_range) | **GET** /transactions/{startlevel}/{stoplevel} | Get All Transactions for a specific Level-Range


# **get_transaction**
> Transaction get_transaction(transaction_hash => $transaction_hash)

Get Transaction

Get a specific Transaction

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::TransactionApi->new();
my $transaction_hash = 'transaction_hash_example'; # string | The hash of the Transaction to retrieve

eval { 
    my $result = $api_instance->get_transaction(transaction_hash => $transaction_hash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransactionApi->get_transaction: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **transaction_hash** | **string**| The hash of the Transaction to retrieve | 

### Return type

[**Transaction**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_transactions_recent**
> ARRAY[Transaction] get_transactions_recent()

Returns the last 50 Transactions

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::TransactionApi->new();

eval { 
    my $result = $api_instance->get_transactions_recent();
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransactionApi->get_transactions_recent: $@\n";
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ARRAY[Transaction]**](Transaction.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **transactions_all**
> TransactionRange transactions_all(page => $page, order => $order, limit => $limit)

Get All Transactions

Get all Transactions

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::TransactionApi->new();
my $page = 3.4; # Number | Pagination, 200 tx per page max
my $order = 'order_example'; # string | ASC or DESC
my $limit = 56; # int | Results per Page

eval { 
    my $result = $api_instance->transactions_all(page => $page, order => $order, limit => $limit);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransactionApi->transactions_all: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **page** | **Number**| Pagination, 200 tx per page max | [optional] 
 **order** | **string**| ASC or DESC | [optional] 
 **limit** | **int**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **transactions_by_level_range**
> TransactionRange transactions_by_level_range(startlevel => $startlevel, stoplevel => $stoplevel, page => $page, order => $order, limit => $limit)

Get All Transactions for a specific Level-Range

Get all Transactions for a specific Level-Range

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::TransactionApi->new();
my $startlevel = 3.4; # Number | lowest blocklevel to return
my $stoplevel = 3.4; # Number | highest blocklevel to return
my $page = 3.4; # Number | Pagination, 200 tx per page max
my $order = 'order_example'; # string | ASC or DESC
my $limit = 56; # int | Results per Page

eval { 
    my $result = $api_instance->transactions_by_level_range(startlevel => $startlevel, stoplevel => $stoplevel, page => $page, order => $order, limit => $limit);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling TransactionApi->transactions_by_level_range: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **startlevel** | **Number**| lowest blocklevel to return | 
 **stoplevel** | **Number**| highest blocklevel to return | 
 **page** | **Number**| Pagination, 200 tx per page max | [optional] 
 **order** | **string**| ASC or DESC | [optional] 
 **limit** | **int**| Results per Page | [optional] 

### Return type

[**TransactionRange**](TransactionRange.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


# TezexClient::OperationApi

## Load the API package
```perl
use TezexClient::Object::OperationApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_operation**](OperationApi.md#get_operation) | **GET** /operation/{operation_hash} | Get Operation


# **get_operation**
> Operation get_operation(operation_hash => $operation_hash)

Get Operation

Get a specific Operation

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::OperationApi->new();
my $operation_hash = 'operation_hash_example'; # string | The hash of the Operation to retrieve

eval { 
    my $result = $api_instance->get_operation(operation_hash => $operation_hash);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling OperationApi->get_operation: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **operation_hash** | **string**| The hash of the Operation to retrieve | 

### Return type

[**Operation**](Operation.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


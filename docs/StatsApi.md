# TezexClient::StatsApi

## Load the API package
```perl
use TezexClient::Object::StatsApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_statistics**](StatsApi.md#get_statistics) | **GET** /stats/{group}/{stat}/{period} | Get Statistics
[**get_stats_overview**](StatsApi.md#get_stats_overview) | **GET** /stats/overview | Returns some basic Info


# **get_statistics**
> ARRAY[Stats] get_statistics(group => $group, stat => $stat, period => $period, start_time => $start_time, end_time => $end_time)

Get Statistics

Get Statistics

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::StatsApi->new();
my $group = 'group_example'; # string | Block, Transaction, etc
my $stat = 'stat_example'; # string | 
my $period = 'period_example'; # string | 
my $start_time = DateTime->from_epoch(epoch => str2time('2013-10-20T19:20:30+01:00')); # DateTime | 
my $end_time = DateTime->from_epoch(epoch => str2time('2013-10-20T19:20:30+01:00')); # DateTime | 

eval { 
    my $result = $api_instance->get_statistics(group => $group, stat => $stat, period => $period, start_time => $start_time, end_time => $end_time);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling StatsApi->get_statistics: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **group** | **string**| Block, Transaction, etc | 
 **stat** | **string**|  | 
 **period** | **string**|  | 
 **start_time** | **DateTime**|  | [optional] 
 **end_time** | **DateTime**|  | [optional] 

### Return type

[**ARRAY[Stats]**](Stats.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_stats_overview**
> StatsOverview get_stats_overview()

Returns some basic Info

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::StatsApi->new();

eval { 
    my $result = $api_instance->get_stats_overview();
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling StatsApi->get_stats_overview: $@\n";
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**StatsOverview**](StatsOverview.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


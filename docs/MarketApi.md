# TezexClient::MarketApi

## Load the API package
```perl
use TezexClient::Object::MarketApi;
```

All URIs are relative to *http://betaapi.tezex.info/v2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**candlestick**](MarketApi.md#candlestick) | **GET** /price/{denominator}/{numerator}/{period} | Candlestick Data
[**ticker**](MarketApi.md#ticker) | **GET** /ticker/{numerator} | Get Ticker for a specific Currency


# **candlestick**
> ARRAY[Candlestick] candlestick(denominator => $denominator, numerator => $numerator, period => $period)

Candlestick Data

Returns CandleStick Prices

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::MarketApi->new();
my $denominator = 'denominator_example'; # string | which currency
my $numerator = 'numerator_example'; # string | to which currency
my $period = 'period_example'; # string | Timeframe of one candle

eval { 
    my $result = $api_instance->candlestick(denominator => $denominator, numerator => $numerator, period => $period);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling MarketApi->candlestick: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **denominator** | **string**| which currency | 
 **numerator** | **string**| to which currency | 
 **period** | **string**| Timeframe of one candle | 

### Return type

[**ARRAY[Candlestick]**](Candlestick.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **ticker**
> Ticker ticker(numerator => $numerator)

Get Ticker for a specific Currency

Returns BTC, USD, EUR and CNY Prices

### Example 
```perl
use Data::Dumper;

my $api_instance = TezexClient::MarketApi->new();
my $numerator = 'numerator_example'; # string | The level of the Blocks to retrieve

eval { 
    my $result = $api_instance->ticker(numerator => $numerator);
    print Dumper($result);
};
if ($@) {
    warn "Exception when calling MarketApi->ticker: $@\n";
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **numerator** | **string**| The level of the Blocks to retrieve | 

### Return type

[**Ticker**](Ticker.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json, application/xml
 - **Accept**: application/xml, application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


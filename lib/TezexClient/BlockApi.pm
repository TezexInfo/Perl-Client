=begin comment

TezosAPI

BETA Tezos API, this may change frequently

OpenAPI spec version: 0.0.2
Contact: office@bitfly.at
Generated by: https://github.com/swagger-api/swagger-codegen.git

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

=end comment

=cut

#
# NOTE: This class is auto generated by the swagger code generator program. 
# Do not edit the class manually.
# Ref: https://github.com/swagger-api/swagger-codegen
#
package TezexClient::BlockApi;

require 5.6.0;
use strict;
use warnings;
use utf8; 
use Exporter;
use Carp qw( croak );
use Log::Any qw($log);

use TezexClient::ApiClient;
use TezexClient::Configuration;

use base "Class::Data::Inheritable";

__PACKAGE__->mk_classdata('method_documentation' => {});

sub new {
    my $class   = shift;
    my (%self) = (
        'api_client' => TezexClient::ApiClient->instance,
        @_
    );

    #my $self = {
    #    #api_client => $options->{api_client}
    #    api_client => $default_api_client
    #}; 

    bless \%self, $class;

}


#
# blocks_all
#
# Get All Blocks 
# 
# @param Number $page Pagination, 200 tx per page max (optional)
# @param string $order ASC or DESC (optional)
# @param int $limit Results per Page (optional)
{
    my $params = {
    'page' => {
        data_type => 'Number',
        description => 'Pagination, 200 tx per page max',
        required => '0',
    },
    'order' => {
        data_type => 'string',
        description => 'ASC or DESC',
        required => '0',
    },
    'limit' => {
        data_type => 'int',
        description => 'Results per Page',
        required => '0',
    },
    };
    __PACKAGE__->method_documentation->{ 'blocks_all' } = { 
    	summary => 'Get All Blocks ',
        params => $params,
        returns => 'BlocksAll',
        };
}
# @return BlocksAll
#
sub blocks_all {
    my ($self, %args) = @_;

    # parse inputs
    my $_resource_path = '/blocks/all';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # query params
    if ( exists $args{'page'}) {
        $query_params->{'page'} = $self->{api_client}->to_query_value($args{'page'});
    }

    # query params
    if ( exists $args{'order'}) {
        $query_params->{'order'} = $self->{api_client}->to_query_value($args{'order'});
    }

    # query params
    if ( exists $args{'limit'}) {
        $query_params->{'limit'} = $self->{api_client}->to_query_value($args{'limit'});
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('BlocksAll', $response);
    return $_response_object;
}

#
# blocks_by_level
#
# Get All Blocks for a specific Level
# 
# @param Number $level The level of the Blocks to retrieve, includes abandoned (required)
{
    my $params = {
    'level' => {
        data_type => 'Number',
        description => 'The level of the Blocks to retrieve, includes abandoned',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'blocks_by_level' } = { 
    	summary => 'Get All Blocks for a specific Level',
        params => $params,
        returns => 'ARRAY[Block]',
        };
}
# @return ARRAY[Block]
#
sub blocks_by_level {
    my ($self, %args) = @_;

    # verify the required parameter 'level' is set
    unless (exists $args{'level'}) {
      croak("Missing the required parameter 'level' when calling blocks_by_level");
    }

    # parse inputs
    my $_resource_path = '/blocks/{level}';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'level'}) {
        my $_base_variable = "{" . "level" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'level'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Block]', $response);
    return $_response_object;
}

#
# blocks_by_level_range
#
# Get All Blocks for a specific Level-Range
# 
# @param Number $startlevel lowest blocklevel to return (required)
# @param Number $stoplevel highest blocklevel to return (required)
{
    my $params = {
    'startlevel' => {
        data_type => 'Number',
        description => 'lowest blocklevel to return',
        required => '1',
    },
    'stoplevel' => {
        data_type => 'Number',
        description => 'highest blocklevel to return',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'blocks_by_level_range' } = { 
    	summary => 'Get All Blocks for a specific Level-Range',
        params => $params,
        returns => 'BlockRange',
        };
}
# @return BlockRange
#
sub blocks_by_level_range {
    my ($self, %args) = @_;

    # verify the required parameter 'startlevel' is set
    unless (exists $args{'startlevel'}) {
      croak("Missing the required parameter 'startlevel' when calling blocks_by_level_range");
    }

    # verify the required parameter 'stoplevel' is set
    unless (exists $args{'stoplevel'}) {
      croak("Missing the required parameter 'stoplevel' when calling blocks_by_level_range");
    }

    # parse inputs
    my $_resource_path = '/blocks/{startlevel}/{stoplevel}';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'startlevel'}) {
        my $_base_variable = "{" . "startlevel" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'startlevel'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    # path params
    if ( exists $args{'stoplevel'}) {
        my $_base_variable = "{" . "stoplevel" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'stoplevel'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('BlockRange', $response);
    return $_response_object;
}

#
# get_block
#
# Get Block By Blockhash
# 
# @param string $blockhash The hash of the Block to retrieve (required)
{
    my $params = {
    'blockhash' => {
        data_type => 'string',
        description => 'The hash of the Block to retrieve',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_block' } = { 
    	summary => 'Get Block By Blockhash',
        params => $params,
        returns => 'Block',
        };
}
# @return Block
#
sub get_block {
    my ($self, %args) = @_;

    # verify the required parameter 'blockhash' is set
    unless (exists $args{'blockhash'}) {
      croak("Missing the required parameter 'blockhash' when calling get_block");
    }

    # parse inputs
    my $_resource_path = '/block/{blockhash}';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'blockhash'}) {
        my $_base_variable = "{" . "blockhash" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'blockhash'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('Block', $response);
    return $_response_object;
}

#
# get_block_delegations
#
# Get Delegations of a Block
# 
# @param string $blockhash Blockhash (required)
{
    my $params = {
    'blockhash' => {
        data_type => 'string',
        description => 'Blockhash',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_block_delegations' } = { 
    	summary => 'Get Delegations of a Block',
        params => $params,
        returns => 'ARRAY[Delegation]',
        };
}
# @return ARRAY[Delegation]
#
sub get_block_delegations {
    my ($self, %args) = @_;

    # verify the required parameter 'blockhash' is set
    unless (exists $args{'blockhash'}) {
      croak("Missing the required parameter 'blockhash' when calling get_block_delegations");
    }

    # parse inputs
    my $_resource_path = '/block/{blockhash}/operations/delegations';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'blockhash'}) {
        my $_base_variable = "{" . "blockhash" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'blockhash'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Delegation]', $response);
    return $_response_object;
}

#
# get_block_endorsements
#
# Get Endorsements of a Block
# 
# @param string $blockhash Blockhash (required)
{
    my $params = {
    'blockhash' => {
        data_type => 'string',
        description => 'Blockhash',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_block_endorsements' } = { 
    	summary => 'Get Endorsements of a Block',
        params => $params,
        returns => 'ARRAY[Endorsement]',
        };
}
# @return ARRAY[Endorsement]
#
sub get_block_endorsements {
    my ($self, %args) = @_;

    # verify the required parameter 'blockhash' is set
    unless (exists $args{'blockhash'}) {
      croak("Missing the required parameter 'blockhash' when calling get_block_endorsements");
    }

    # parse inputs
    my $_resource_path = '/block/{blockhash}/operations/endorsements';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'blockhash'}) {
        my $_base_variable = "{" . "blockhash" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'blockhash'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Endorsement]', $response);
    return $_response_object;
}

#
# get_block_operations_sorted
#
# Get operations of a block, sorted
# 
# @param string $blockhash The hash of the Block to retrieve (required)
{
    my $params = {
    'blockhash' => {
        data_type => 'string',
        description => 'The hash of the Block to retrieve',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_block_operations_sorted' } = { 
    	summary => 'Get operations of a block, sorted',
        params => $params,
        returns => 'BlockOperationsSorted',
        };
}
# @return BlockOperationsSorted
#
sub get_block_operations_sorted {
    my ($self, %args) = @_;

    # verify the required parameter 'blockhash' is set
    unless (exists $args{'blockhash'}) {
      croak("Missing the required parameter 'blockhash' when calling get_block_operations_sorted");
    }

    # parse inputs
    my $_resource_path = '/block/{blockhash}/operations';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'blockhash'}) {
        my $_base_variable = "{" . "blockhash" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'blockhash'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('BlockOperationsSorted', $response);
    return $_response_object;
}

#
# get_block_originations
#
# Get Originations of a Block
# 
# @param string $blockhash Blockhash (required)
{
    my $params = {
    'blockhash' => {
        data_type => 'string',
        description => 'Blockhash',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_block_originations' } = { 
    	summary => 'Get Originations of a Block',
        params => $params,
        returns => 'ARRAY[Origination]',
        };
}
# @return ARRAY[Origination]
#
sub get_block_originations {
    my ($self, %args) = @_;

    # verify the required parameter 'blockhash' is set
    unless (exists $args{'blockhash'}) {
      croak("Missing the required parameter 'blockhash' when calling get_block_originations");
    }

    # parse inputs
    my $_resource_path = '/block/{blockhash}/operations/originations';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'blockhash'}) {
        my $_base_variable = "{" . "blockhash" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'blockhash'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Origination]', $response);
    return $_response_object;
}

#
# get_block_transaction
#
# Get Transactions of Block
# 
# @param string $blockhash Blockhash (required)
{
    my $params = {
    'blockhash' => {
        data_type => 'string',
        description => 'Blockhash',
        required => '1',
    },
    };
    __PACKAGE__->method_documentation->{ 'get_block_transaction' } = { 
    	summary => 'Get Transactions of Block',
        params => $params,
        returns => 'ARRAY[Transaction]',
        };
}
# @return ARRAY[Transaction]
#
sub get_block_transaction {
    my ($self, %args) = @_;

    # verify the required parameter 'blockhash' is set
    unless (exists $args{'blockhash'}) {
      croak("Missing the required parameter 'blockhash' when calling get_block_transaction");
    }

    # parse inputs
    my $_resource_path = '/block/{blockhash}/operations/transactions';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    # path params
    if ( exists $args{'blockhash'}) {
        my $_base_variable = "{" . "blockhash" . "}";
        my $_base_value = $self->{api_client}->to_path_value($args{'blockhash'});
        $_resource_path =~ s/$_base_variable/$_base_value/g;
    }

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Transaction]', $response);
    return $_response_object;
}

#
# recent_blocks
#
# returns the last 25 blocks
# 
{
    my $params = {
    };
    __PACKAGE__->method_documentation->{ 'recent_blocks' } = { 
    	summary => 'returns the last 25 blocks',
        params => $params,
        returns => 'ARRAY[Block]',
        };
}
# @return ARRAY[Block]
#
sub recent_blocks {
    my ($self, %args) = @_;

    # parse inputs
    my $_resource_path = '/blocks/recent';
    $_resource_path =~ s/{format}/json/; # default format to json

    my $_method = 'GET';
    my $query_params = {};
    my $header_params = {};
    my $form_params = {};

    # 'Accept' and 'Content-Type' header
    my $_header_accept = $self->{api_client}->select_header_accept('application/xml', 'application/json');
    if ($_header_accept) {
        $header_params->{'Accept'} = $_header_accept;
    }
    $header_params->{'Content-Type'} = $self->{api_client}->select_header_content_type('application/json', 'application/xml');

    my $_body_data;
    # authentication setting, if any
    my $auth_settings = [qw()];

    # make the API Call
    my $response = $self->{api_client}->call_api($_resource_path, $_method,
                                           $query_params, $form_params,
                                           $header_params, $_body_data, $auth_settings);
    if (!$response) {
        return;
    }
    my $_response_object = $self->{api_client}->deserialize('ARRAY[Block]', $response);
    return $_response_object;
}

1;
